import React, {Component} from 'react';

class Films extends Component {
    shouldComponentUpdate(nextProps) {
        return nextProps.text !== this.props.text;
    }

    render() {
        return (
            <div id="container">
                <div className="txt">
                    <input onChange={this.props.change} value={this.props.text} type="text"/>
                    <span className="removeTask" onClick={this.props.removeTask}>x</span>
                </div>
            </div>
        )
    }
}

export default Films;