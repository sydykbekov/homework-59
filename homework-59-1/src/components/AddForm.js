import React, {Component} from 'react';

class AddForm extends Component {
    render() {
        return (
            <form>
                <input placeholder="Film ..." value={this.props.text} onChange={this.props.handleChange} id="task" type="text"/>
                <button onClick={this.props.handleClick} id="add">Add</button>
            </form>
        );
    }
}

export default AddForm;