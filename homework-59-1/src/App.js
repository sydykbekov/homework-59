import React, {Component} from 'react';
import './App.css';
import AddForm from './components/AddForm';
import Films from './components/Films';

class App extends Component {
    state = {
        tasks: [],
        text: ''
    };

    componentDidMount() {
        let tasks = JSON.parse(localStorage.getItem('array'));
        this.setState({tasks});
    }

    handleChange = (event) => {
        this.setState({text: event.target.value});
    };

    handleClick = (e) => {
        e.preventDefault();
        if (this.state.text !== '') {
            let obj = {
                text: this.state.text,
                id: Date.now()
            };
            const tasks = [...this.state.tasks];
            tasks.unshift(obj);
            this.setState({tasks, text: ''});
            let string = JSON.stringify(tasks);
            localStorage.setItem('array', string);
        } else {
            alert('Please add a task!');
        }
    };

    changePost = (event, index) => {
        let taskIndex;
        const tasks = [...this.state.tasks];
        tasks.forEach((item, i) => item.id === index ? taskIndex = i : null);

        const task = {...this.state.tasks[taskIndex]};
        task.text = event.target.value;

        tasks[taskIndex] = task;
        localStorage.setItem('array', JSON.stringify(tasks));
        this.setState({tasks});
    };

    removeTask = (id) => {
        const index = this.state.tasks.findIndex(div => div.id === id);
        const tasks = [...this.state.tasks];
        tasks.splice(index, 1);
        localStorage.setItem('array', JSON.stringify(tasks));
        this.setState({tasks});
    };

    render() {
        return (
            <div className="App">
                <AddForm text={this.state.text} handleClick={this.handleClick} handleChange={this.handleChange}/>
                {this.state.tasks.map((task) =>
                    <Films key={task.id} change={(event) => this.changePost(event, task.id)} removeTask={() => this.removeTask(task.id)} text={task.text}/>
                )}
            </div>
        );
    }
}

export default App;