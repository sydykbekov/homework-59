import React, {Component} from 'react';
import './App.css';
import Jokes from "./components/Jokes";
import NewJokes from "./components/NewJokes";

class App extends Component {
    state = {
        array: []
    };

    componentDidMount() {
        const URL = 'https://api.chucknorris.io/jokes/random';
        let promises = [];
        for (let i = 0; i < 5; i++) {
            promises.push(fetch(URL));
        }
        Promise.all(promises).then(jokes => {
            return Promise.all(jokes.map(joke => joke.json()));
        }).then(jokes => {
            let array = jokes.map(item => {
                return {
                    id: item.id,
                    value: item.value
                }
            });
            this.setState({array});
        });
    }

    getNewJokes = () => {
        this.componentDidMount();
    };

    render() {
        return (
            <div className="App">
                <NewJokes click={this.getNewJokes} />
                <Jokes array={this.state.array} />
            </div>
        );
    }
}

export default App;
