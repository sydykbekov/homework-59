import React from 'react';

const Jokes = props => (
    <div>
        {props.array.map(joke => <div key={joke.id} className="joke">{joke.value}</div>)}
    </div>
);

export default Jokes;